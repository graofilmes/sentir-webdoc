import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    creditos: '',
    togglePass: false,
    toggleLoad: true,
    passaporte: {
      ver: false,
      cheirar: false,
      tocar: false,
      saborear: false,
      ouvir: false
    },
    saida: {
      ver: false,
      cheirar: false,
      tocar: false,
      saborear: false,
      ouvir1: false,
      ouvir2: false
    },
    lastChange: ''
  },
  mutations: {
    setDestino: (state, destino) => state.passaporte[destino] = true,
    setSaida: (state, saida) => state.saida[saida] = true,
    setPage: (state, page) => state.creditos = page,
    togglePass: (state) => state.togglePass = !state.togglePass,
    toggleLoad: (state) => state.toggleLoad = !state.toggleLoad,
    makeChange: (state, destino) => state.lastChange = destino,
  },
  actions: {
    checkin({ commit, state }, destino){
      console.log(destino);
      if(state.togglePass == false ){
        commit('togglePass');
      } else if(state.creditos !== ''){
        commit('setPage', '');
      }
      if(state.toggleLoad == false){
        commit('toggleLoad');
      }
      commit('setDestino', destino);
      commit('makeChange', destino);
      setTimeout(() => {
        if(state.togglePass == true && state.creditos == ''){
          commit('togglePass');
        }
      }, 2500)
    },
    checkout({ commit, state }, destino){
      if(state.togglePass == false ){
        commit('togglePass');
      } else if(state.creditos !== ''){
        commit('setPage', '');
      }
      if(state.toggleLoad == false){
        commit('toggleLoad');
      }
      commit('setSaida', destino);
      commit('makeChange', 'e_' + destino);
      setTimeout(() => {
        if(state.togglePass == true && state.creditos == ''){
          commit('togglePass');
        }
      }, 2500)
    },
    showPass({ commit, state }){
      if(state.creditos !== ''){
        commit('setPage', '')
      }
      commit('togglePass');
    },
    closePass({ commit, state }){
      if(state.togglePass == true){
        commit('togglePass');
      }
    },
    closeLoad({ commit, state }){
      if(state.toggleLoad == true){
        commit('toggleLoad');
      }
    },
    changePage({ commit }, page){
      commit('setPage', page)
    }
  },
  modules: {
  },
  getters: {
    destinos: (state) => state.passaporte,
    extras: (state) => state.saida,
    pass: (state) => state.togglePass,
    load: (state) => state.toggleLoad,
    creditos: (state) => state.creditos,
    change: (state) => state.lastChange
  }
})
