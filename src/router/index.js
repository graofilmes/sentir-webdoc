import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: () => import('../views/Home.vue'),
    children: [
        {
          path: '/',
          name: 'main',
          // route level code-splitting
          // this generates a separate chunk (about.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          component: () => import('../views/Main.vue')
        },
        { path: '/cheirar', name: 'Cheirar', component: () => import('../views/Cheirar.vue') },
        { path: '/ouvir', name: 'Ouvir', component: () => import('../views/Ouvir.vue') },
        { path: '/saborear', name: 'Saborear', component: () => import('../views/Saborear.vue') },
        { path: '/tocar', name: 'Tocar', component: () => import('../views/Tocar.vue') },
        { path: '/ver', name: 'Ver', component: () => import('../views/Ver.vue') },
        { path: '/galeria', name: 'Galeria', component: () => import('../views/extras/Galeria.vue') },
        { path: '/receitas', name: 'Receitas', component: () => import('../views/extras/Receitas.vue') },
        { path: '/cheiros', name: 'Cheiros', component: () => import('../views/extras/Cheiros.vue') },
        { path: '/quizz', name: 'Quizz', component: () => import('../views/extras/Quizz.vue') },
      ]
  },
]

const router = new VueRouter({
  routes
})

export default router
